package fr.grunberg.katawatertank;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

public class KataWaterTankTest {
	
	@Test
	public void basic3Houses() {
		assertEquals(2, KataWaterTank.fillPlots("-H-HH--"));
	}
	@Test
	public void basicSinglePlot() {
		assertEquals(-1, KataWaterTank.fillPlots("H"));
	}
	@Test
	public void basic5Houses1Empty() {
		assertEquals(-1, KataWaterTank.fillPlots("HH-HH"));
	}
	@Test
	public void basic10Plots5Houses() {
		assertEquals(3, KataWaterTank.fillPlots("-H-H-H-H-H"));
	}
	@Test
	public void nullTest() {
		assertEquals(-1, KataWaterTank.fillPlots(null));
	}
	@Test
	public void emptyStringTest() {
		assertEquals(0, KataWaterTank.fillPlots(""));
	}
	@Test
	public void basicNoHouse() {
		assertEquals(0, KataWaterTank.fillPlots("-"));
	}
	@Test
	public void incorrectString() {
		assertEquals(-1, KataWaterTank.fillPlots("h-"));
	}
}

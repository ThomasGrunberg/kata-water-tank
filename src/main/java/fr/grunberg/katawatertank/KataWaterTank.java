package fr.grunberg.katawatertank;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class KataWaterTank {
	public static int fillPlots(String housingPlots) {
		if(housingPlots == null)
			return -1;
		
		if(!housingPlots
				.replace("H", "")
				.replace("-", "").isEmpty())
			return -1;
		
		List<Land> street = translateString(housingPlots);
		if(street.isEmpty())
			return 0;
		
		int numberOfHouses = countHouses(street);
		if(numberOfHouses == 0)
			return 0;
		
		street = fillEmptyPlotsWithTanks(street);
		if(!isStreetDrained(street))
			return -1;
		
		int numberOfWaterTanks = countWaterTanks(street);
		
		if(numberOfHouses == 1 && numberOfWaterTanks == 1)
			return numberOfWaterTanks;
		
		int numberOfWaterTanksFromPreviousCycle = Integer.MAX_VALUE;
		while(countWaterTanks(street) < numberOfWaterTanksFromPreviousCycle) {
			numberOfWaterTanksFromPreviousCycle = countWaterTanks(street);
			street = removeOneWaterTankAndStayDrained(street);
		}
		
		return countWaterTanks(street);
	}
	
	private static boolean canRemoveOneWaterTankAndStayDrained(List<Land> street, int index) {
		if(street.get(index) != Land.WATER_TANK)
			return false;
		if(index == 1 && street.get(0) == Land.HOUSE)
			return false;
		if(index == street.size()-2 && street.get(street.size()-1) == Land.HOUSE)
			return false;
		if(index >= 2 && street.get(index-1) == Land.HOUSE && street.get(index-2) != Land.WATER_TANK)
			return false;
		if(index <= street.size()-2 && street.get(index+1) == Land.HOUSE && street.get(index+2) != Land.WATER_TANK)
			return false;
		
		return true;
	}

	private static List<Land> removeOneWaterTankAndStayDrained(List<Land> street) {
		street = street.stream().collect(Collectors.toList());
		for(int i = 0; i < street.size(); i++) {
			if(canRemoveOneWaterTankAndStayDrained(street, i)) {
				street.set(i, Land.EMPTY_PLOT);
				return street;
			}
		}
		return street;
	}
	
	private static int countHouses(List<Land> street) {
		return (int) street.stream()
				.filter(l -> l == Land.HOUSE)
				.count();
	}

	private static int countWaterTanks(List<Land> street) {
		return (int) street.stream()
				.filter(l -> l == Land.WATER_TANK)
				.count();
	}
	
	private static boolean isStreetDrained(List<Land> street) {
		for(int i = 0; i < street.size(); i++) {
			if(street.get(i) == Land.HOUSE) {
				boolean waterTankFound = false;
				if(i > 0 && street.get(i-1) == Land.WATER_TANK)
					waterTankFound = true;
				if(i < street.size()-1 && street.get(i+1) == Land.WATER_TANK)
					waterTankFound = true;
				if(!waterTankFound)
					return false;
			}
		}
		return true;
	}
	
	private static List<Land> fillEmptyPlotsWithTanks(List<Land> street) {
		street = street.stream().collect(Collectors.toList());
		for(int i = 0; i < street.size(); i++) {
			if(street.get(i) == Land.EMPTY_PLOT)
				street.set(i, Land.WATER_TANK);
		}
		return street;
	}
	
	private static List<Land> translateString(String housingPlots) {
		List<Land> housingLandPlots = new ArrayList<>(housingPlots.length());
		for(String housingPlot : housingPlots.split("")) {
			housingLandPlots.add(
					switch(housingPlot) {
						case "H" -> Land.HOUSE;
						case "-" -> Land.EMPTY_PLOT;
						default -> Land.EMPTY_PLOT;
			});
		}
		return housingLandPlots;
	}
}

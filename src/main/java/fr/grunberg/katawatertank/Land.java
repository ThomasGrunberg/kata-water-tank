package fr.grunberg.katawatertank;

public enum Land {
	HOUSE("H"),
	WATER_TANK("T"),
	EMPTY_PLOT("-");
	
	private final String symbol;
	
	Land(String symbol) {
		this.symbol = symbol;
	}

	public String getSymbol() {
		return symbol;
	}
}
